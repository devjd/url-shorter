### System Prerequisite 
- httpd 
- PHP 7.4
- composer
- mysql

### Steps to setup project
- Clone the project from git 
- Create one database with no tables in it in mysql
- Add .env to the project and add database connection data in it.
- fire <code>composer install</code> command. It will install all packages in vendor folder of project.
- fire <code>php artisan migrate</code> command. It will create required tables in database.
- fire <code>php artisan serve</code> command if running in local to start the project.