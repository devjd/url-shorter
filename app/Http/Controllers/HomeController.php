<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Url;

class HomeController extends Controller
{
    // FUNCTION TO GENERATE URL KEY
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function index()
    {
        $data = array();
        return view('home', $data);
    }

    public function createLink(Request $request)
    {
        // CHECK FOR VALIDATION
        $validator = Validator::make($request->all(), [
            'url' => 'required|url',
        ],[
            'url.required' => "Please enter URL first!",
            'url.url' => "Entered URL does not seem to be validssss one!"
        ]);
        if ($validator->fails()) {
            return redirect('/')
                        ->withErrors($validator)
                        ->withInput();
        }

        // ENTERED URL
        $url_val = $request->url;

        // CHECK IF URL EXISTS IN DATABASE
        $url_data = Url::where('url','=',$url_val)->first();

        // ARRAY TO STORE
        $value_arr = array(
            'url' => $url_val
        );

        // CREATE OR UPDATE RECORD
        if(empty($url_data)){
            $generated_url_key = $this->generateRandomString(10);
            $value_arr['url_key'] = $generated_url_key;
            Url::create($value_arr);
        }else{
            $generated_url_key = $url_data->url_key;
            Url::where('id','=',$url_data->id)->update($value_arr);
        }

        return redirect('/')->withSuccess("generated_url_key_succ")->with( ['generated_url_key' => $generated_url_key]);
    }

    public function redirectLink($url_key = "")
    {
        // CHECK IF URL EXISTS WITH KEY
        $url_data = Url::where('url_key','=',$url_key)->first();
                    
        // REDIRECT TO ENTERED URL
        if(!empty($url_data)){
            return redirect($url_data->url);
        }else{
            return redirect('/');
        }
        
    }
}
