$.validator.setDefaults({
    errorClass: "url-form-error-lbl",
});
$(document).ready(function(){
    // validate signup form on keyup and submit
    $("#frm_url_short").validate({
        rules: {
            url: {
                required: true,
                url: true
            },
        },
        messages: {
            //
        },
        errorPlacement: function (error, element) {
            $(document).find('label.url-form-error-lbl').remove();
            $(document).find('#frm_url_short').before(error);
        }
    });
});
