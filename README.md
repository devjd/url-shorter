## Key points

- The project is made in Laravel framework of PHP. 
- It simply stores the entered url by user in database table created in MYSQL. 
- Tables are managed using concept of migration
- Server-side and client-side validations both are added.
- The whole functionality contains use of Laravel's provided solutions.

## My highlights upon your judgmental criteria
- Git : The repository is public. You can check commits and source there. <a href="https://bitbucket.org/devjd/url-shorter/src/master/" target="_blank">THE REPO</a>
- Code organization : -
- Objectivity : -
- Maintainability : -
- Scalability : -

### PLEASE NOTE THAT DUE TO PROBLEM IN MY OWN MACHINE, I HAD TO WORK ON OTHER'S MACHINE TO EXECUTE THE TASK. SO YOU MAY FIND DIFFRENT AUTHOR NAME IN COMMIT.  