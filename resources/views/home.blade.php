<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>URL Shorter</title>
    <!-- INITIALIZE CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <!-- INITIALIZE JS -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/validate.min.js')}}"></script>
    <script src="{{asset('js/custom.js')}}"></script>
</head>
<body>
    <div class="main-card-wrapper" id="url_main_wrapper">
        <h1 class="intro-text">Paste your hectic URL and we will generate a short redirection URL for you!</h1>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(session('success'))
            @if(session('success') == 'generated_url_key_succ' && Session::get('generated_url_key'))
            <div class="alert alert-success">
                <p>Url created successfully! Please use below url to open your entered url.</p>
                <p><a href="{{url('/'.Session::get('generated_url_key'))}}" target="_blank">{{url('/'.Session::get('generated_url_key'))}}</a></p>
            </div>
            @endif
        @endif
        <form method="post" id="frm_url_short" action="{{route('createLink')}}">
            @csrf
            <div>
                <textarea name="url" class="textarea" id="url" cols="30" rows="10">{{ old('url') }}</textarea>
            </div>
            <button class="button" type="submit" id="btn_generate">Generate</button>
        </form>
    </div>
</body>
</html>